## g2172fua_v1_ga_oj_m01_qd_r_Natv-user 11 RP1A.201005.001 2112060202 release-keys
- Manufacturer: chuanglian
- Platform: ums312
- Codename: mblu10
- Brand: mblu
- Flavor: g2172fua_v1_ga_oj_m01_qd_r_Natv-user
- Release Version: 11
- Id: RP1A.201005.001
- Incremental: 2112060202
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: ChuangLian/mblu_10_CN/mblu10:11/RP1A.201005.001/2112060202:user/release-keys
- OTA version: 
- Branch: g2172fua_v1_ga_oj_m01_qd_r_Natv-user-11-RP1A.201005.001-2112060202-release-keys
- Repo: mblu_mblu10_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
