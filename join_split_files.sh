#!/bin/bash

cat system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null >> system/system/priv-app/DirectService/DirectService.apk
rm -f system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null
cat system/system/priv-app/FlymeMusic/FlymeMusic.apk.* 2>/dev/null >> system/system/priv-app/FlymeMusic/FlymeMusic.apk
rm -f system/system/priv-app/FlymeMusic/FlymeMusic.apk.* 2>/dev/null
cat system/system/priv-app/FlymeMusic/oat/arm64/FlymeMusic.odex.* 2>/dev/null >> system/system/priv-app/FlymeMusic/oat/arm64/FlymeMusic.odex
rm -f system/system/priv-app/FlymeMusic/oat/arm64/FlymeMusic.odex.* 2>/dev/null
cat system/system/priv-app/Video/Video.apk.* 2>/dev/null >> system/system/priv-app/Video/Video.apk
rm -f system/system/priv-app/Video/Video.apk.* 2>/dev/null
cat system/system/priv-app/FlymeBrowser/FlymeBrowser.apk.* 2>/dev/null >> system/system/priv-app/FlymeBrowser/FlymeBrowser.apk
rm -f system/system/priv-app/FlymeBrowser/FlymeBrowser.apk.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat product/custom/3rd-party/apk/UCBrowser_CMCC/UCBrowser_CMCC.apk.* 2>/dev/null >> product/custom/3rd-party/apk/UCBrowser_CMCC/UCBrowser_CMCC.apk
rm -f product/custom/3rd-party/apk/UCBrowser_CMCC/UCBrowser_CMCC.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/amap/amap.apk.* 2>/dev/null >> product/custom/3rd-party/apk/amap/amap.apk
rm -f product/custom/3rd-party/apk/amap/amap.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/cmvideo/cmvideo.apk.* 2>/dev/null >> product/custom/3rd-party/apk/cmvideo/cmvideo.apk
rm -f product/custom/3rd-party/apk/cmvideo/cmvideo.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/kugou/kugou.apk.* 2>/dev/null >> product/custom/3rd-party/apk/kugou/kugou.apk
rm -f product/custom/3rd-party/apk/kugou/kugou.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null >> product/custom/3rd-party/apk/baidu/baidu.apk
rm -f product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk.* 2>/dev/null >> product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk
rm -f product/custom/3rd-party/apk/BaiduNetDisk/BaiduNetDisk.apk.* 2>/dev/null
cat product/app/webview/webview.apk.* 2>/dev/null >> product/app/webview/webview.apk
rm -f product/app/webview/webview.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
